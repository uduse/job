// Job.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <set>
#include <iomanip>

using namespace std;

struct Job
{
	int weight;
	int length;
	long double ratio;
	int diff;

	Job( int weight, int length ):weight( weight ), length( length )
	{
		ratio = static_cast<long double>( weight ) / length;
		diff = weight - length;
	}

	void print() const;

};

void Job::print() const
{
	cout << setw( 4 ) << weight << setw( 4 ) << length << endl;
}

//bool operator<( const Job &lhs, const Job &rhs )
//{
//	if ( lhs.diff  == rhs.diff )
//	{
//		return lhs.weight > rhs.weight ? true : false;
//	}
//	return lhs.diff > rhs.diff;
//}

bool operator<( const Job &lhs, const Job &rhs )
{
	return lhs.ratio > rhs.ratio;
}

int _tmain( int argc, _TCHAR* argv[] )
{
	int temp2;
	int temp;
	int numJobs;
	//ifstream input( "1.txt" );
	ifstream input( "jobs.txt" );
	multiset<Job> jobs;


	input >> numJobs;

	while ( input >> temp >> temp2 )
	{
		jobs.insert( Job( temp, temp2 ) );
	}

	int time = 0;
	unsigned long long sum = 0;
	set<Job>::iterator itr;
	for ( itr = jobs.begin(); itr != jobs.end(); itr++ )
	{
		time += itr->length;
		sum += itr->weight * time;
	}
	cout << "sum: " << sum << endl;

	return 0;
}

